package Exercise1;

public class SignToAscii {
	char sign;

	public SignToAscii(char sign) {
		this.sign = sign;
	}

	public int Convert() {
		int b = (int) sign;
		return b;
	}

}
