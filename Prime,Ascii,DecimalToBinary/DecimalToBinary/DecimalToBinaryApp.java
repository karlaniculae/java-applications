package AdditionalExercise2;

import java.util.Scanner;

public class DecimalToBinaryApp {
	public void Ap(int count, int n) {
		int i = 0;
		int[] vector = new int[count];
		while (n > 0) {
			vector[i] = n % 2;

			n = n / 2;
			i++;
		}
		for (i = count - 1; i >= 0; i--) {
			System.out.print(vector[i]);
		}
	}

	public static void main(String args[]) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the number:");
		int n = in.nextInt();

		int count = 0;
		int copie = n;
		while (copie > 0) {
			copie = copie / 2;
			count++;
		}
		DecimalToBinaryApp m = new DecimalToBinaryApp();
		m.Ap(count, n);

	}

}
