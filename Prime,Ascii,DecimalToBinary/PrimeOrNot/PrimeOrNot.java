package AdditionalExercise1;

public class PrimeOrNot {

	public void Prime(int p) {
		int div = 0;
		int ok1 = 0;
		for (div = 2; div <= Math.sqrt(p); div++) {
			if (p % div == 0) {
				System.out.println("Not prime!");
				ok1 = 1;
				break;
			}
		}
		if (ok1 == 0) {
			System.out.println("Prime!");
		}
	}
}
