package AdditionalExercise1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class PrimeOrNotApp {
	public static void main(String args[]) {

		Scanner sc = new Scanner(System.in);
		try {
			System.out.print("Enter an integer:");
			int number = sc.nextInt();
			PrimeOrNot m = new PrimeOrNot();
			m.Prime(number);

		} catch (InputMismatchException e) {
			System.out.println("Not an integer, crashing down");

		}
	}

}

