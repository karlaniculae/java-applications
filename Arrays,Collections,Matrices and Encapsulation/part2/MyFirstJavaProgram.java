package exercitiul2;

import java.util.Scanner;

public class MyFirstJavaProgram {
	public static void main(String args[]) {
		while (true) {

			String[] words = { "This", "is", "a", "java", "scramble", "game." };
			System.out.println("The scrambled phrase is:");
			System.out.println(
					words[4] + " " + words[0] + " " + words[2] + " " + words[3] + " " + words[1] + " " + words[5]);
			for (int i = 0; i < words.length; i++) {

				Scanner in = new Scanner(System.in);
				if (i == 0) {
					System.out.println("Which is the first word from the phrase:");
				} else
					System.out.println("Which is the next word from the phrase:");
				String Word = in.nextLine();
				System.out.println("You typed:" + Word);
				boolean isCorrect = Word.equals(words[i]);
				if (isCorrect) {
					System.out.print("It's correct.");
					if (i == 5) {
						System.out.println("You won!");
						System.out.println("The correct phrase was:" + words[0] + " " + words[1] + " " + words[2] + " "
								+ words[3] + " " + words[4] + " " + words[5]);
					}

				} else {
					System.out.print("Try again!");
					i--;

				}
			}
			break;
		}

	}

}
