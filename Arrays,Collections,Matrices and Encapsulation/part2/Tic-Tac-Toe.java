import java.util.Scanner;

public class X0App {
	public static String Winner(char matrix[][]) {
		String line1 = " ";
		String line2 = " ";
		String line3 = " ";
		String result = "no";
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				line1 += matrix[i][j];

			}
			line1 += " ";
		}
		for (int j = 0; j < 3; j++) {
			for (int i = 0; i < 3; i++) {
				line2 += matrix[i][j];

			}
			line2 += " ";
		}
		for (int i = 0; i < 3; i++) {
			line3 += matrix[i][i];

		}
		line3 += " ";
		int j = 2;
		for (int i = 0; i < 3; i++) {
			line3 += matrix[i][j - i];

		}
		line3 += " ";

		String[] divide1 = line1.split(" ");
		String[] divide2 = line2.split(" ");
		String[] divide3 = line3.split(" ");
		for (int i = 0; i < divide1.length; i++) {
			if (divide1[i].equals("xxx") || divide2[i].equals("xxx")) {
				result = "Winner is x";

			} else if (divide1[i].equals("000") || divide2[i].equals("000")) {
				result = "Winner is 0";
			}
		}
		for (int i = 0; i < divide3.length; i++) {
			if (divide3[i].equals("xxx")) {
				result = "Winner is x";
			} else if (divide1[i].equals("000")) {
				result = "Winner is 0";
			}

		}
		return result;

	}

	public static void main(String args[]) {
		char[][] matrix = new char[3][3];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				matrix[i][j] = 'r';
			}
		}
		System.out.println("Hello");
		Scanner in = new Scanner(System.in);
		System.out.println("Let's start the game!" + "Choose WHO STARTS: x or 0");
		char player1 = in.next().charAt(0);
		char player2 = '0';

		int row = 0;
		int colon = 0;
		int flag = 0;
		int ok4 = 0;
		if (player1 == 'x') {
			player2 = '0';
		} else if (player1 == '0') {
			player2 = 'x';
		}
		while (ok4 == 0) {
			int ok1 = 0;

			while (ok1 == 0) {
				in = new Scanner(System.in);
				System.out.println("Player1 Insert row from 0 to 2:");
				row = in.nextInt();
				in = new Scanner(System.in);
				System.out.println("Player1 Insert colon from 0 to 2:");
				colon = in.nextInt();

				if (matrix[row][colon] == 'r') {
					matrix[row][colon] = player1;

					ok1 = 1;
				} else {
					System.out.println("TRY AGAIN!");
				}
			}
			String line = Winner(matrix);
			if (line.equals("Winner is x")) {
				System.out.println(line);
				System.out.println("Good job!");
				break;

			} else if (line.equals("Winner is 0")) {
				System.out.println(line);
				System.out.println("Good job!");
				break;
			} else if (line.equals("no")) {
				for (int i = 0; i < 3; i++) {
					for (int j = 0; j < 3; j++)
						if (matrix[i][j] == 'r') {
							flag = 1;
						}
				}
			}
			if (flag == 0) {
				System.out.println("DRAW");
				System.out.println("Good job!");
				break;
			} else {
				int ok2 = 0;
				while (ok2 == 0) {
					in = new Scanner(System.in);
					System.out.println("Player2 Insert row from 0 to 2:");
					row = in.nextInt();
					in = new Scanner(System.in);
					System.out.println("Player2 Insert colon from 0 to 2:");
					colon = in.nextInt();
					if (matrix[row][colon] == 'r') {
						matrix[row][colon] = player2;
						ok2 = 1;
					} else {
						System.out.println("TRY AGAIN!");
					}
				}
				line = Winner(matrix);
				if (line.equals("Winner is x")) {
					System.out.println(line);
					System.out.println("Good job!");
					break;

				} else if (line.equals("Winner is 0")) {
					System.out.println(line);
					System.out.println("Good job!");
					break;
				} else if (line.equals("no")) {
					for (int i = 0; i < 3; i++) {
						for (int j = 0; j < 3; j++)
							if (matrix[i][j] == 'r') {
								flag = 1;
							}
					}
				}
				if (flag == 0) {
					System.out.println("DRAW");
					ok4 = 1;
					System.out.println("Good job!");
					break;

				} else {
					ok4 = 0;
				}
			}
		}
	}
}
