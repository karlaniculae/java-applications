package exercitiul3;

import java.util.Scanner;

public class MatrixCalculator {
	public static void main(String args[]) {
		Scanner in = new Scanner(System.in);
		System.out.println("Choose:add/negative/substract/multiply");
		String operation = in.nextLine();
		in = new Scanner(System.in);
		System.out.println("Insert number of lines:");
		int lines = in.nextInt();
		in = new Scanner(System.in);
		System.out.println("Insert number of columns:");
		int columns = in.nextInt();
		double[][] matrix1 = new double[lines][columns];
		double[][] matrix2 = new double[lines][columns];
		double[][] matrix3 = new double[lines][columns];
		if (operation.equals("add") || operation.equals("substract")) {
			System.out.println("Insert numbers for the first matrix");
			for (int i = 0; i < lines; i++) {
				for (int j = 0; j < columns; j++) {
					in = new Scanner(System.in);
					System.out.println("Insert numbers ");
					int no = in.nextInt();
					matrix1[i][j] = no;
				}
			}
			System.out.println("Insert numbers for the second matrix");
			for (int i = 0; i < lines; i++) {
				for (int j = 0; j < columns; j++) {
					in = new Scanner(System.in);
					int no = in.nextInt();
					matrix2[i][j] = no;
				}
			}
		} else {
			System.out.println("Insert numbers for the  matrix");
			for (int i = 0; i < lines; i++) {
				for (int j = 0; j < columns; j++) {
					in = new Scanner(System.in);
					int no = in.nextInt();
					matrix1[i][j] = no;

				}
			}

		}
		int n = 5;
		if (operation.equals("add")) {
			n = 0;
		} else if (operation.equals("negative")) {
			n = 1;
		} else if (operation.equals("substract")) {
			n = 2;
		} else {
			n = 3;
		}
		switch (n) {
		case 0:
			for (int i = 0; i < lines; i++) {
				for (int j = 0; j < columns; j++) {
					matrix3[i][j] = matrix1[i][j] + matrix2[i][j];

				}
			}
			break;
		case 1:
			for (int i = 0; i < lines; i++) {
				for (int j = 0; j < columns; j++) {
					matrix3[i][j] = -matrix1[i][j];
				}
			}
			break;
		case 2:
			for (int i = 0; i < lines; i++) {
				for (int j = 0; j < columns; j++) {
					matrix3[i][j] = matrix1[i][j] - matrix2[i][j];
				}
			}
			break;
		case 3:
			in = new Scanner(System.in);
			System.out.println("Insert the constant:");
			double constant = in.nextDouble();
			for (int i = 0; i < lines; i++) {
				for (int j = 0; j < columns; j++) {
					matrix3[i][j] = constant * matrix1[i][j];
				}
			}
			break;

		}
		System.out.println("The answer is:");
		for (int i = 0; i < lines; i++) {
			for (int j = 0; j < columns; j++) {
				System.out.print(matrix3[i][j] + " ");
			}
			System.out.println();
		}

	}
}
