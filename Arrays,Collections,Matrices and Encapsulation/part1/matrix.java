import java.util.Scanner;

public class matrix {
	public static void main(String[] args) {
		Scanner sc;
		int[][] matrix = new int[4][5];
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 5; j++) {
				sc = new Scanner(System.in);
				int no = sc.nextInt();
				matrix[i][j] = no;
			}
		}
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 5; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
	}
}
