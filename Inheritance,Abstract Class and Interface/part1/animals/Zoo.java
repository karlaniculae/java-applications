package animals;

public class Zoo {
	Animal[] animals;

	public Zoo(Animal[] animals) {
		this.animals = animals;
	}

	public void describeAllAnimals() {
		for (int i = 0; i < animals.length; i++) {
			System.out.print(animals[i].name + " ");
			animals[i].eat();
		//	if(animals[i] instanceof Lion) {
		//			System.out.println("Is the KING!");
		//	}
		//	if(animals[i] instanceof Zebra || animals[i] instanceof Giraffe) {
		//		System.out.println("is herbivorous!");
			if(animals[i] instanceof Herbivorous) {
				System.out.println("is herbivorous");
				((Herbivorous  ) animals[i]).sayNumberOfLegs();
			} 
			if(animals[i] instanceof Carnivorous) {
				System.out.println("is carnivorous");
			} 
		}
		}

}