package animals;

public class ZooApp {

	public static void main(String[] args) {
		Animal[] animals=new Animal[6];
		Giraffe g1=new Giraffe("Mac");
		animals[0]=g1;
		Giraffe g2=new Giraffe("MacGiro");
		animals[1]=g2;
		Zebra z1=new Zebra("Zebro");
		animals[2]=z1;
		
		Lion l=new Lion("Simba");
		animals[3]=l;
		Penguin p=new Penguin("POP");
		animals[4]=p;
		Bear b=new Bear("Olaf");
		animals[5]=b;
		
	Zoo zoo=new Zoo(animals);
	zoo.describeAllAnimals();}

}
