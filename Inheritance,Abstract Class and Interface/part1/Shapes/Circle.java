package Shapes;

public class Circle extends Shape{

	public Circle(double length, double area, double perimeter) {
		super(length, area, perimeter);
		
	}

	@Override
	public String toString() {
		return "Circle [length=" + length + ", area=" + area + ", perimeter=" + perimeter + "]";
	}

}
