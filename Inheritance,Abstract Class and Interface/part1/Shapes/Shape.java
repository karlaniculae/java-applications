package Shapes;

public class Shape  {
 double length;
 double area;
 double perimeter;
public Shape(double length, double area, double perimeter) {
	super();
	this.length = length;
	this.area = area;
	this.perimeter = perimeter;
}

public String toString() {
	return "Shape [length=" + length + ", area=" + area + ", perimeter=" + perimeter + "]";
}
}
