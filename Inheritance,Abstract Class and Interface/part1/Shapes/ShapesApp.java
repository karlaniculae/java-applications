package Shapes;

public class ShapesApp {
	public static void main(String args[]) {
		Circle c1=new Circle(2,3,4);
		Circle c2=new Circle(5,6,8);
		Circle c3=new Circle(7,4,9);
		Square s1=new Square(6,7,9);
		Rectangle r1=new Rectangle(5,6,7,9);
		Rectangle r2=new Rectangle(50,60,70,90);
		Circle[] circleArr=new Circle[3];
		circleArr[0]=c1;
		circleArr[1]=c2;
		circleArr[2]=c3;
		Shape[] shapeArr= new Shape[3];
		shapeArr[0]=c1;
		shapeArr[1]=s1;
		shapeArr[2]=r1;
		
		System.out.println(c1);
		System.out.println(s1);
		System.out.println(r1);
		for (int i=0;i<3;i++) {
			System.out.println(shapeArr[i]);
	}
}}
