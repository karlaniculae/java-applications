package Shapes;

public class Square extends Shape {

	public Square(double length, double area, double perimeter) {
		super(length, area, perimeter);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Square [length=" + length + ", area=" + area + ", perimeter=" + perimeter + "]";
	}

	
	
}
