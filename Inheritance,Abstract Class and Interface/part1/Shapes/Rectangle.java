package Shapes;

public class Rectangle extends Shape {
	
	double width;
	public Rectangle(double length,double width, double area, double perimeter) {
		super(length, area, perimeter);
		this.width=width;
	}
	@Override
	public String toString() {
		return "Rectangle [width=" + width + ", length=" + length + ", area=" + area + ", perimeter=" + perimeter + "]";
	}

	
}
