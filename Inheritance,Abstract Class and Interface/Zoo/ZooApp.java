package Zoo;

import java.util.Scanner;

public class ZooApp {
	public static void main(String agrs[]) {

		boolean exit1 = true;
		int l = 0, b = 0, p = 0, c = 0, pen = 0;
		Scanner sc, in;
		Animals[] animals = new Animals[99];
		Lions[] lionvec = new Lions[99];
		Parrots[] parrotvec = new Parrots[99];
		Bears[] bearvec = new Bears[99];
		Crocodiles[] crocodilevec = new Crocodiles[99];
		Penguins[] penguinvec = new Penguins[99];
		int i = 0;
		String animal = null, input;
		String allanimal = null;
		int value2 = 0;
		while (exit1) {
			in = new Scanner(System.in);
			System.out.println("You are an administrator/visitor?");
			System.out.println("Write EXIT if you want to exit!");
			String person = in.nextLine();
			if (person.equals("administrator")) {
				boolean valueoftruth = true;
				while (valueoftruth) {
					sc = new Scanner(System.in);

					System.out.println("Please choose an animaL:Lion/Penguin/Crocodile/Parrot/Bear");
					System.out.println("Write EXIT if you have finished!");
					input = sc.nextLine();
					if (input.equals("Lion")) {
						sc = new Scanner(System.in);
						System.out.println("Please choose a name:");
						String name = sc.nextLine();
						sc = new Scanner(System.in);
						System.out.println("Please write the age:");
						int age = sc.nextInt();
						Lions lion = new Lions(name, age);
						lionvec[i] = lion;
						animals[i] = lion;
						i++;
						l++;

					} else if (input.equals("Penguin")) {
						sc = new Scanner(System.in);
						System.out.println("Please choose a name:");
						String name = sc.nextLine();
						sc = new Scanner(System.in);
						System.out.println("Please write the age:");
						int age = sc.nextInt();
						Penguins penguin = new Penguins(name, age);
						penguinvec[i] = penguin;
						animals[i] = penguin;
						i++;
						pen++;

					} else if (input.equals("Crocodile")) {
						sc = new Scanner(System.in);
						System.out.println("Please choose a name:");
						String name = sc.nextLine();
						sc = new Scanner(System.in);
						System.out.println("Please write the age:");
						int age = sc.nextInt();
						Crocodiles crocodile = new Crocodiles(name, age);
						crocodilevec[i] = crocodile;
						animals[i] = crocodile;

						i++;
						c++;

					} else if (input.equals("Parrot")) {
						sc = new Scanner(System.in);
						System.out.println("Please choose a name:");
						String name = sc.nextLine();
						sc = new Scanner(System.in);
						System.out.println("Please write the age:");
						int age = sc.nextInt();
						Parrots parrot = new Parrots(name, age);
						parrotvec[i] = parrot;
						animals[i] = parrot;

						i++;
						p++;
					} else if (input.equals("Bear")) {
						sc = new Scanner(System.in);
						System.out.println("Please choose a name:");
						String name = sc.nextLine();
						sc = new Scanner(System.in);
						System.out.println("Please write the age:");
						int age = sc.nextInt();
						Bears bear = new Bears(name, age);
						bearvec[i] = bear;
						animals[i] = bear;

						i++;
						b++;
					} else if (input.equals("EXIT")) {
						valueoftruth = false;
					} else {

						System.out.println("Try again and enter a valid animal!");
					}

				}
			}

			else if (person.equals("visitor")) {
				boolean exit = true;
				while (exit) {
					sc = new Scanner(System.in);
					System.out.println("Please choose an animaL:Lion/Penguin/Crocodile/Parrot/Bear/ALL");
					System.out.println("Write EXIT if you have finished!");

					animal = sc.nextLine();
					if (animal.equals("ALL")) {
						Zoo zoo = new Zoo(animals);
						zoo.describeAllAnimals(i);
					}

					else if (animal.equals("Lion") || animal.equals("Penguin") || animal.equals("Crocodile")
							|| animal.equals("Parrot") || animal.equals("Lion")) {

						if (animal.equals("Lion")) {

							Zoo zoo = new Zoo(lionvec);
							zoo.describeAllAnimals(l);

						} else if (animal.equals("Penguin")) {
							Zoo zoo = new Zoo(penguinvec);
							zoo.describeAllAnimals(pen);

						} else if (animal.equals("Crocodile")) {
							Zoo zoo = new Zoo(crocodilevec);
							zoo.describeAllAnimals(c);

						} else if (animal.equals("Parrot")) {
							Zoo zoo = new Zoo(parrotvec);
							zoo.describeAllAnimals(p);

						} else if (animal.equals("Bear")) {
							Zoo zoo = new Zoo(bearvec);
							zoo.describeAllAnimals(b);
						}

					} else if (animal.equals("EXIT")) {
						exit = false;

					} else {

						System.out.println("Try again and enter a valid input!");
					}
				}
			}

			else if (person.equals("EXIT")) {
				System.out.println("Thank you for using our application!");
				exit1 = false;
			}
		}
	}
}
