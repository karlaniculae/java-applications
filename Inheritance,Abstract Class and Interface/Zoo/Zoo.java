package Zoo;

public class Zoo {
	Animals[] animals;

	public Zoo(Animals[] animals) {

		this.animals = animals;
	}

	public void describeAllAnimals(int number) {
		int j = number;
		if (j == 0) {
			System.out.println("There are no animals like this now");

		}

		else if (j != 0) {
			for (int i = 0; i < j; i++) {

				if (animals[i] instanceof Crocodiles) {
					System.out.println("Name of the animal:Crocodile");
				}
				if (animals[i] instanceof Lions) {
					System.out.println("Name of the animal:Lion");
				}
				if (animals[i] instanceof Penguins) {
					System.out.println("Name of the animal:Penguin");
				}
				if (animals[i] instanceof Parrots) {
					System.out.println("Name of the animal:Parrot");
				}
				if (animals[i] instanceof Bears) {
					System.out.println("Name of the animal:Bear");
				}
				System.out.println("Name:" + animals[i].name + " ");
				System.out.println("Age:" + animals[i].age + " years");
				if (animals[i] instanceof Crocodiles || animals[i] instanceof Lions) {
					System.out.println("Origin:Asia");
				}
				if (animals[i] instanceof Reptiles) {
					System.out.println("Type:Reptile");
				}
				if (animals[i] instanceof Mammals) {
					System.out.println("Type:Mammal");
				}
				if (animals[i] instanceof Birds) {
					System.out.println("Type:Bird");
				}
			}
		}
	}
}
