package LibraryBooksManagementApplication;

public class Kids extends Books implements ActionAndAdventure, Drama, Mystery, Romance {

	public Kids(String name, double price, String date, Authors author, int quantity, String genre) {
		super(name, price, date, author, quantity, genre);
	}
}
