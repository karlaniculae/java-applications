package LibraryBooksManagementApplication;

import java.util.InputMismatchException;
import java.util.Scanner;

public class LibraryApp {
	public static void main(String[] args) {
		Books[] book = new Books[199];
		boolean exit = true;
		int i = 0;

		String genre = null;

		double price = 0;

		int quantity = 0;

		String date = null;

		String authordata = null;
		while (exit) {
			Scanner sc = new Scanner(System.in);
			System.out.println("Administrator/Reader/EXIT");
			String ocupation = sc.nextLine();

			if (ocupation.equals("Administrator")) {
				boolean exit1 = true;
				while (exit1) {
					int ok2 = 0;
					sc = new Scanner(System.in);
					System.out.println("Category:Kids/Adults/Blind/EXIT");
					String intro = sc.nextLine();

					if (intro.equals("Kids")) {
						boolean exit2 = true;
						while (exit2) {
							boolean y = true;
							String name = null;
							while (y) {
								sc = new Scanner(System.in);
								System.out.println("The name of the book:");
								name = sc.nextLine();
								VerifyInput v = new VerifyInput();
								y = v.verify(name);
							}
							String authorname = null;
							y = true;
							while (y) {
								sc = new Scanner(System.in);
								System.out.println("The name of the author: ");
								authorname = sc.nextLine();
								VerifyInput v = new VerifyInput();
								y = v.verify(authorname);
							}
							String authorsurname = null;
							y = true;
							while (y) {
								sc = new Scanner(System.in);
								System.out.println("Author's surname:");
								authorsurname = sc.nextLine();
								VerifyInput v = new VerifyInput();
								y = v.verify(authorsurname);
							}
							int ok = 0;
							for (int k = 0; k < i; k++) {
								if (book[k].name.equals(name) && book[k].author.name.equals(authorname)
										&& book[k].author.surname.equals(authorname)) {
									ok = 1;
									System.out.println("You already have entered this book.");
									sc = new Scanner(System.in);
									System.out.println("Do you want to make changes?:Yes/No");
									String input = sc.nextLine();
									if (input.equals("Yes")) {

										boolean value = true;
										while (value) {
											sc = new Scanner(System.in);
											System.out.println("Genre:Action and Adventure/Drama/Mystery/Romance ");
											genre = sc.nextLine();
											if (!(genre.equals("Action and Adventure")) && !(genre.equals("Drama"))
													&& !(genre.equals("Mystery")) && !(genre.equals("Romance"))) {

												System.out.println("Try again!Enter an valid value");
											} else {
												value = false;
											}
										}

										boolean error = true;
										boolean error1 = true;

										while (error1) {
											try {
												sc = new Scanner(System.in);
												System.out.println("Price: ");
												price = sc.nextDouble();
											} catch (InputMismatchException e) {
												System.out.println("You have to introduce numbers!");
												error = false;

											}
											if (error == false) {
												error1 = true;
												error = true;
											} else {
												error1 = false;
											}
										}
										error = true;
										error1 = true;
										while (error1) {
											try {
												sc = new Scanner(System.in);
												System.out.println("Quantity: ");
												quantity = sc.nextInt();
											} catch (Exception e) {
												System.out.println("You have to introduce numbers!");
												error = false;

											}
											if (error == false) {
												error1 = true;
												error = true;
											} else {
												error1 = false;
											}
										}

										book[k].price = price;
										book[k].quantity = quantity;
										book[k].genre = genre;
									}
								}
							}
							if (ok == 0) {
								boolean value = true;
								while (value) {
									sc = new Scanner(System.in);
									System.out.println("Genre:Action and Adventure/Drama/Mystery/Romance ");
									genre = sc.nextLine();
									if (!(genre.equals("Action and Adventure")) && !(genre.equals("Drama"))
											&& !(genre.equals("Mystery")) && !(genre.equals("Romance"))) {

										System.out.println("Try again!Enter an valid value");
									} else {
										value = false;
									}
								}
								boolean error = true;
								boolean error1 = true;

								while (error1) {
									try {
										sc = new Scanner(System.in);
										System.out.println("Price: ");
										price = sc.nextDouble();
									} catch (InputMismatchException e) {
										System.out.println("You have to introduce numbers!");
										error = false;

									}
									if (error == false) {
										error1 = true;
										error = true;
									} else {
										error1 = false;
									}
								}
								error = true;
								error1 = true;
								while (error1) {
									try {
										sc = new Scanner(System.in);
										System.out.println("Quantity: ");
										quantity = sc.nextInt();
									} catch (Exception e) {
										System.out.println("You have to introduce numbers!");
										error = false;

									}
									if (error == false) {
										error1 = true;
										error = true;
									} else {
										error1 = false;
									}
								}
								y = true;
								while (y) {
									sc = new Scanner(System.in);
									System.out.println("Release date:zz.zz.zzzz");
									date = sc.nextLine();
									VerifyInput v = new VerifyInput();
									y = v.verify(date);
								}
								y = true;
								while (y) {
									sc = new Scanner(System.in);
									System.out.println("The author's date of birth:zz.zz.zzzz");
									authordata = sc.nextLine();
									VerifyInput v = new VerifyInput();
									y = v.verify(authordata);
								}

								Kids kid = new Kids(name, price, date,
										new Authors(authorname, authorsurname, authordata), quantity, genre);

								book[i] = kid;
								i++;
							}
							boolean exit6 = true;
							while (exit6) {
								sc = new Scanner(System.in);
								System.out.println("Kids/EXIT");
								String option = sc.nextLine();
								if (option.equals("Kids")) {

									exit6 = false;
								} else if (option.equals("EXIT")) {
									exit2 = false;
									exit6 = false;
								} else {
									System.out.println("Try again!Enter a valid input!");
								}
							}
						}

					} else if (intro.equals("Adults")) {
						boolean exit2 = true;
						while (exit2) {
							boolean y = true;
							String name = null;
							while (y) {
								sc = new Scanner(System.in);
								System.out.println("The name of the book:");
								name = sc.nextLine();
								VerifyInput v = new VerifyInput();
								y = v.verify(name);
							}
							String authorname = null;
							y = true;
							while (y) {
								sc = new Scanner(System.in);
								System.out.println("The name of the author: ");
								authorname = sc.nextLine();
								VerifyInput v = new VerifyInput();
								y = v.verify(authorname);
							}
							String authorsurname = null;
							y = true;
							while (y) {
								sc = new Scanner(System.in);
								System.out.println("Author's surname:");
								authorsurname = sc.nextLine();
								VerifyInput v = new VerifyInput();
								y = v.verify(authorsurname);
							}
							int ok = 0;
							for (int k = 0; k < i; k++) {
								if (book[k].name.equals(name) && book[k].author.name.equals(authorname)
										&& book[k].author.surname.equals(authorname)) {
									ok = 1;
									System.out.println("You already have entered this book.");
									sc = new Scanner(System.in);
									System.out.println("Do you want to make changes?:Yes/No");
									String input = sc.nextLine();
									if (input.equals("Yes")) {
										boolean value = true;
										while (value) {
											sc = new Scanner(System.in);
											System.out.println(
													"Genre:Action and Adventure/Drama/Mystery/Romance//Horror");
											genre = sc.nextLine();
											if (!(genre.equals("Action and Adventure")) && !(genre.equals("Drama"))
													&& !(genre.equals("Mystery")) && !(genre.equals("Romance"))
													&& !(genre.equals("Horror"))) {

												System.out.println("Try again!Enter an valid value");
											} else {
												value = false;
											}
										}
										boolean error = true;
										boolean error1 = true;

										while (error1) {
											try {
												sc = new Scanner(System.in);
												System.out.println("Price: ");
												price = sc.nextDouble();
											} catch (InputMismatchException e) {
												System.out.println("You have to introduce numbers!");
												error = false;

											}
											if (error == false) {
												error1 = true;
												error = true;
											} else {
												error1 = false;
											}
										}
										error = true;
										error1 = true;
										while (error1) {
											try {
												sc = new Scanner(System.in);
												System.out.println("Quantity: ");
												quantity = sc.nextInt();
											} catch (Exception e) {
												System.out.println("You have to introduce numbers!");
												error = false;

											}
											if (error == false) {
												error1 = true;
												error = true;
											} else {
												error1 = false;
											}
										}

										book[k].price = price;
										book[k].quantity = quantity;
										book[k].genre = genre;
									}
								}
							}
							if (ok == 0) {
								boolean value = true;
								while (value) {
									sc = new Scanner(System.in);
									System.out.println("Genre:Action and Adventure/Drama/Mystery/Romance/Horror ");
									genre = sc.nextLine();
									if (!(genre.equals("Action and Adventure")) && !(genre.equals("Drama"))
											&& !(genre.equals("Mystery")) && !(genre.equals("Romance"))
											&& !(genre.equals("Horror"))) {

										System.out.println("Try again!Enter an valid value");
									} else {
										value = false;
									}
								}
								boolean error = true;
								boolean error1 = true;

								while (error1) {
									try {
										sc = new Scanner(System.in);
										System.out.println("Price: ");
										price = sc.nextDouble();
									} catch (InputMismatchException e) {
										System.out.println("You have to introduce numbers!");
										error = false;

									}
									if (error == false) {
										error1 = true;
										error = true;
									} else {
										error1 = false;
									}
								}
								error = true;
								error1 = true;
								while (error1) {
									try {
										sc = new Scanner(System.in);
										System.out.println("Quantity: ");
										quantity = sc.nextInt();
									} catch (Exception e) {
										System.out.println("You have to introduce numbers!");
										error = false;

									}
									if (error == false) {
										error1 = true;
										error = true;
									} else {
										error1 = false;
									}
								}
								y = true;
								while (y) {
									sc = new Scanner(System.in);
									System.out.println("Release date:zz.zz.zzzz");
									date = sc.nextLine();
									VerifyInput v = new VerifyInput();
									y = v.verify(date);
								}
								y = true;
								while (y) {
									sc = new Scanner(System.in);
									System.out.println("The author's date of birth:zz.zz.zzzz");
									authordata = sc.nextLine();
									VerifyInput v = new VerifyInput();
									y = v.verify(authordata);
								}

								Blind blind = new Blind(name, price, date,
										new Authors(authorname, authorsurname, authordata), quantity, genre);

								book[i] = blind;
								i++;
							}
							boolean exit6 = true;
							while (exit6) {
								sc = new Scanner(System.in);
								System.out.println("Adults/EXIT");
								String option = sc.nextLine();
								if (option.equals("Adults")) {

									exit6 = false;
								} else if (option.equals("EXIT")) {
									exit2 = false;
									exit6 = false;
								} else {
									System.out.println("Try again!Enter a valid input!");
								}
							}
						}

					} else if (intro.equals("Blind")) {
						boolean exit2 = true;
						while (exit2) {
							boolean y = true;
							String name = null;
							while (y) {
								sc = new Scanner(System.in);
								System.out.println("The name of the book:");
								name = sc.nextLine();
								VerifyInput v = new VerifyInput();
								y = v.verify(name);
							}
							String authorname = null;
							y = true;
							while (y) {
								sc = new Scanner(System.in);
								System.out.println("The name of the author: ");
								authorname = sc.nextLine();
								VerifyInput v = new VerifyInput();
								y = v.verify(authorname);
							}
							String authorsurname = null;
							y = true;
							while (y) {
								sc = new Scanner(System.in);
								System.out.println("Author's surname:");
								authorsurname = sc.nextLine();
								VerifyInput v = new VerifyInput();
								y = v.verify(authorsurname);
							}
							int ok = 0;
							for (int k = 0; k < i; k++) {
								if (book[k].name.equals(name) && book[k].author.name.equals(authorname)
										&& book[k].author.surname.equals(authorname)) {
									ok = 1;
									System.out.println("You already have entered this book.");
									sc = new Scanner(System.in);
									System.out.println("Do you want to make changes?:Yes/No");
									String input = sc.nextLine();
									if (input.equals("Yes")) {
										boolean value = true;
										while (value) {
											sc = new Scanner(System.in);
											System.out.println(
													"Genre:Action and Adventure/Drama/Mystery/Romance//Horror");
											genre = sc.nextLine();
											if (!(genre.equals("Action and Adventure")) && !(genre.equals("Drama"))
													&& !(genre.equals("Mystery")) && !(genre.equals("Romance"))
													&& !(genre.equals("Horror"))) {

												System.out.println("Try again!Enter an valid value");
											} else {
												value = false;
											}
										}
										boolean error = true;
										boolean error1 = true;

										while (error1) {
											try {
												sc = new Scanner(System.in);
												System.out.println("Price: ");
												price = sc.nextDouble();
											} catch (InputMismatchException e) {
												System.out.println("You have to introduce numbers!");
												error = false;

											}
											if (error == false) {
												error1 = true;
												error = true;
											} else {
												error1 = false;
											}
										}
										error = true;
										error1 = true;
										while (error1) {
											try {
												sc = new Scanner(System.in);
												System.out.println("Quantity: ");
												quantity = sc.nextInt();
											} catch (Exception e) {
												System.out.println("You have to introduce numbers!");
												error = false;

											}
											if (error == false) {
												error1 = true;
												error = true;
											} else {
												error1 = false;
											}
										}

										quantity = sc.nextInt();
										book[k].price = price;
										book[k].quantity = quantity;
										book[k].genre = genre;
									}
								}
							}
							if (ok == 0) {
								boolean value = true;
								while (value) {
									sc = new Scanner(System.in);
									System.out.println("Genre:Action and Adventure/Drama/Mystery/Romance/Horror ");
									genre = sc.nextLine();
									if (!(genre.equals("Action and Adventure")) && !(genre.equals("Drama"))
											&& !(genre.equals("Mystery")) && !(genre.equals("Romance"))
											&& !(genre.equals("Horror"))) {

										System.out.println("Try again!Enter an valid value");
									} else {
										value = false;
									}
								}
								boolean error = true;
								boolean error1 = true;

								while (error1) {
									try {
										sc = new Scanner(System.in);
										System.out.println("Price: ");
										price = sc.nextDouble();
									} catch (InputMismatchException e) {
										System.out.println("You have to introduce numbers!");
										error = false;

									}
									if (error == false) {
										error1 = true;
										error = true;
									} else {
										error1 = false;
									}
								}
								error = true;
								error1 = true;
								while (error1) {
									try {
										sc = new Scanner(System.in);
										System.out.println("Quantity: ");
										quantity = sc.nextInt();
									} catch (Exception e) {
										System.out.println("You have to introduce numbers!");
										error = false;

									}
									if (error == false) {
										error1 = true;
										error = true;
									} else {
										error1 = false;
									}
								}
								y = true;
								while (y) {
									sc = new Scanner(System.in);
									System.out.println("Release date:zz.zz.zzzz");
									date = sc.nextLine();
									VerifyInput v = new VerifyInput();
									y = v.verify(date);
								}
								y = true;
								while (y) {
									sc = new Scanner(System.in);
									System.out.println("The author's date of birth:zz.zz.zzzz");
									authordata = sc.nextLine();
									VerifyInput v = new VerifyInput();
									y = v.verify(authordata);
								}

								Blind blind = new Blind(name, price, date,
										new Authors(authorname, authorsurname, authordata), quantity, genre);

								book[i] = blind;
								i++;
							}
							boolean exit6 = true;
							while (exit6) {
								sc = new Scanner(System.in);
								System.out.println("Blind/EXIT");
								String option = sc.nextLine();
								if (option.equals("Blind")) {

									exit6 = false;
								} else if (option.equals("EXIT")) {
									exit2 = false;
									exit6 = false;
								} else {
									System.out.println("Try again!Enter a valid input!");
								}
							}
						}

					} else if (intro.equals("EXIT")) {
						exit1 = false;
						ok2 = 1;
					} else if (ok2 == 0) {
						System.out.println("Try again!Enter a valid input.");
					}

				}
			}

			else if (ocupation.equals("Reader")) {
				boolean exit3 = true;
				while (exit3) {
					boolean exit4 = true;
					while (exit4) {
						boolean y = true;
						sc = new Scanner(System.in);
						System.out.println("Searching criteria:By book name/By author name/By release date/EXIT");
						String choice = sc.nextLine();

						if (choice.equals("By book name")) {

							String name = null;
							while (y) {
								sc = new Scanner(System.in);
								System.out.println("The name of the book:");
								name = sc.nextLine();
								VerifyInput v = new VerifyInput();
								y = v.verify(name);
							}
							Describe describe = new Describe(book);
							describe.describeAll(i, name, 0);
						} else if (choice.equals("By author name")) {
							boolean n = true;
							while (n) {
								sc = new Scanner(System.in);
								System.out.println("Full name/First name/Last name/EXIT");
								String choice2 = sc.nextLine();
								if (choice2.equals("Full name")) {
									String authorname = null;
									y = true;
									while (y) {
										sc = new Scanner(System.in);
										System.out.println("The name of the author: ");
										authorname = sc.nextLine();
										VerifyInput v = new VerifyInput();
										y = v.verify(authorname);
									}
									Describe describe = new Describe(book);
									describe.describeAll(i, authorname, 1);
								} else if (choice2.equals("First name")) {
									String authorname = null;
									y = true;
									while (y) {
										sc = new Scanner(System.in);
										System.out.println("The name of the author: ");
										authorname = sc.nextLine();
										VerifyInput v = new VerifyInput();
										y = v.verify(authorname);
									}
									Describe describe = new Describe(book);
									describe.describeAll(i, authorname, 0);
								} else if (choice2.equals("Last name")) {
									String authorname = null;
									y = true;
									while (y) {
										sc = new Scanner(System.in);
										System.out.println("The name of the author: ");
										authorname = sc.nextLine();
										VerifyInput v = new VerifyInput();
										y = v.verify(authorname);
									}
									Describe describe = new Describe(book);
									describe.describeAll(i, authorname, 0);
								} else if (choice2.equals("EXIT")) {
									n = false;
								} else {
									System.out.println("Try again!Enter a valid input!");
								}
							}
						} else if (choice.equals("By release date")) {
							y = true;
							while (y) {
								sc = new Scanner(System.in);
								System.out.println("Release date:zz.zz.zzzz");
								date = sc.nextLine();
								VerifyInput v = new VerifyInput();
								y = v.verify(date);
							}
							Describe describe = new Describe(book);
							describe.describeAll(i, date, 0);
						} else if (choice.equals("EXIT")) {
							exit4 = false;
						} else {
							System.out.println("Try again!Enter a valid input!");
						}
					}
					boolean exit5 = true;
					while (exit5) {
						sc = new Scanner(System.in);
						System.out.println("Reader/EXIT");
						String choice2 = sc.nextLine();
						if (choice2.equals("Reader")) {

							exit5 = false;
						} else if (choice2.equals("EXIT")) {
							exit3 = false;
							exit5 = false;
						} else {
							System.out.println("Try again!Enter a valid input!");
						}
					}

				}
			} else if (ocupation.equals("EXIT")) {
				exit = false;
			} else {
				System.out.println("Try again!Enter a valid input!");
			}
		}
	}
}
