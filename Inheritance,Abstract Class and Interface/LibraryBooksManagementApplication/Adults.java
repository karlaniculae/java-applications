package LibraryBooksManagementApplication;

public class Adults extends Books implements ActionAndAdventure, Drama, Horror, Mystery, Romance {

	public Adults(String name, double price, String date, Authors author, int quantity, String genre) {
		super(name, price, date, author, quantity, genre);
	}
}
