package LibraryBooksManagementApplication;

public class Describe {
	Books[] books;

	public Describe(Books[] books) {
		this.books = books;
	}

	public void describeAll(int length, String string, int ok) {
		if (length == 0) {
			System.out.println("Sorry!There is no book yet.");
		}
		for (int i = 0; i < length; i++) {
			if (books[i].name.equals(string)) {
				System.out.println("The name of the book:" + books[i].name);
				System.out.println("The name of the author:" + books[i].author.name + " " + books[i].author.surname);
				System.out.println("Book genre:" + books[i].genre);
				System.out.println("Author's birth date:" + books[i].author.date);
				System.out.println("The release date of the book:" + books[i].date);
				System.out.println("The price of the book:" + books[i].price);
				System.out.println("Books quantity:" + books[i].quantity);
			} else if ((books[i].date.equals(string))) {
				System.out.println("The name of the book:" + books[i].name);
				System.out.println("The name of the author:" + books[i].author.name + " " + books[i].author.surname);
				System.out.println("Book genre:" + books[i].genre);
				System.out.println("Author's birth date:" + books[i].author.date);
				System.out.println("The release date of the book:" + books[i].date);
				System.out.println("The price of the book:" + books[i].price);
				System.out.println("Books quantity:" + books[i].quantity);
			} else if (ok == 1) {
				String[] divide1 = string.split(" ");

				if (divide1[0].equals(books[i].author.name) && divide1[1].equals(books[i].author.surname)) {
					System.out.println("The name of the book:" + books[i].name);
					System.out
							.println("The name of the author:" + books[i].author.name + " " + books[i].author.surname);
					System.out.println("Book genre:" + books[i].genre);
					System.out.println("Author's birth date:" + books[i].author.date);
					System.out.println("The release date of the book:" + books[i].date);
					System.out.println("The price of the book:" + books[i].price);
					System.out.println("Books quantity:" + books[i].quantity);
				}

			} else if (ok == 0) {
				if (books[i].author.name.equals(string) || books[i].author.surname.equals(string)) {
					System.out.println("The name of the book:" + books[i].name);
					System.out
							.println("The name of the author:" + books[i].author.name + " " + books[i].author.surname);
					System.out.println("Book genre:" + books[i].genre);
					System.out.println("Author's birth date:" + books[i].author.date);
					System.out.println("The release date of the book:" + books[i].date);
					System.out.println("The price of the book:" + books[i].price);
					System.out.println("Books quantity:" + books[i].quantity);
				}

			} else {
				System.out.println("There is nothing like this in stock!");
			}
		}
	}
}
