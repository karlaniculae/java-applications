package LibraryBooksManagementApplication;

import java.util.Scanner;

public class VerifyInput {
	public boolean verify(String string) {
		boolean error = true;
		int ok = 0;
		while (error) {
			System.out.println("You want to enter this:" + string);
			System.out.println("Yes/No");
			Scanner sc = new Scanner(System.in);
			String option = sc.nextLine();
			if (option.equals("Yes")) {
				error = false;
				ok = 1;
			} else if (option.equals("No")) {
				error = false;
				ok = 2;
			} else {
				error = true;
				System.out.println("Try again.Enter a valid input!");
			}

		}
		if (ok == 1) {
			return false;
		} else {
			return true;
		}
	}
}
