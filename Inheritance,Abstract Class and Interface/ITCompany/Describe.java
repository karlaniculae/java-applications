package ITCompany;

public class Describe {
	Employees[] employees;

	public Describe(Employees[] employees) {
		this.employees = employees;
	}

	public void describeAll(int length) {

		if (length == 0) {
			System.out.println("There is no employee add yet");
		}
		for (int i = 0; i < length; i++) {

			System.out.println("Name of the employee:" + employees[i].name + " ");
			System.out.println("Surname of the employee:" + employees[i].surname + " ");
			System.out.println("Age  of the employee:" + employees[i].age + " ");
			System.out.println("Salary of the employee:" + employees[i].salary + " ");
			if (employees[i] instanceof Bonus) {
				System.out.println("Bonus of the employee:");
				((Bonus) employees[i]).bonus();
			}
			if (employees[i].technicallead == true) {
				System.out.println("Technical lead:Yes");
			} else if (employees[i].technicallead == false) {
				System.out.println("Technical lead:No");
			}
			if (employees[i].manager == true) {
				System.out.println("Manager:Yes");
			} else if (employees[i].manager == false) {
				System.out.println("Manager:No");
			}
		}
	}

	public void manager(int length, String name, String surname) {
		if (length == 0) {
			System.out.println("There is no employee add yet");
		}

		for (int i = 0; i < length; i++) {
			if (!(employees[i].name.equals(name)) && !(employees[i].name.equals(surname))) {
				System.out.println("There is no one named:" + name + surname);
			} else if (employees[i].manager && employees[i].name.equals(name) && employees[i].name.equals(surname)) {
				System.out.println("Is already a manager");
			} else if (employees[i].manager) {
				employees[i].manager = false;
			} else if (employees[i].name.equals(name) && employees[i].name.equals(surname)) {
				employees[i].manager = true;
			}
		}
	}
}
