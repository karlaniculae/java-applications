package ITCompany;

public abstract class Employees {
	String name, surname;
	int age;
	double salary;
	boolean manager, technicallead;

	public Employees(String name, String surname, int age, double salary, boolean technicallead, boolean manager) {
		this.name = name;
		this.surname = surname;
		this.age = age;
		this.salary = salary;
		this.technicallead = technicallead;
		this.manager = manager;
	}
}
