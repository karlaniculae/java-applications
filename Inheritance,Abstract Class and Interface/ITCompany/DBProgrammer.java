package ITCompany;

public class DBProgrammer extends Employees implements Bonus {
	double bonus;

	public DBProgrammer(String name, String surname, int age, double salary, double bonus, boolean techincallead,
			boolean manager) {
		super(name, surname, age, salary, techincallead, manager);
		this.bonus = bonus;
	}

	public void bonus() {
		System.out.println(bonus);
	}
}
