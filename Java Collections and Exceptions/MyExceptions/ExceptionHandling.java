package Exceptions;

import java.util.Scanner;

public class ExceptionHandling {
	public static void main(String args[]) {
		try {
			int sum = 0;
			int i = 0;
			boolean exit = true;
			while (exit) {
				Scanner sc = new Scanner(System.in);
				System.out.println("Enter a number");
				try {
					int number = sc.nextInt();

					sum += number;
				} catch (Exception e) {
					System.out.println("Enter an integer");
				}
				i++;
				double medie = sum / i;
				if (i > 13) {
					exit = false;
				}
				System.out.println(medie);
				if (medie > 10) {
					throw new MyException();
				}

			}

		} catch (MyException e) {
			System.out.println("The value is over 1000");
		}
	}
}
