package CountriesApp;

public class Country {
	String name;
	int people;
	String continent;
	public Country(String name, int people, String continent) {
		super();
		this.name = name;
		this.people = people;
		this.continent = continent;
	}
	
	public String toString() {
		return "Country [name=" + name + ", people=" + people + ", continent=" + continent + "]";
	}
	

}
