package PhoneBook;

public class Details {
	String phoneNumber;
	String firstname;
	String lastname;
	String street;
	String number;
	String city;
	String country;
	public Details(String phoneNumber,String lastname,  String firstname, String street, String number, String city,
			String country) {
		super();
		this.phoneNumber = phoneNumber;
		this.firstname = firstname;
		this.lastname = lastname;
		this.street = street;
		this.number = number;
		this.city = city;
		this.country = country;
	}
	@Override
	public String toString() {
		return "Details [phoneNumber=" + phoneNumber + ", firstname=" + firstname + ", lastname=" + lastname
				+ ", street=" + street + ", number=" + number + ", city=" + city + ", country=" + country + "]";
	}
	
	
	

}
