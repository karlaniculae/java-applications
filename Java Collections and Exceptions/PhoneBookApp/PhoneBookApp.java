package PhoneBook;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class PhoneBookApp {
	public static void main(String args[]) {
		List<Details> array = new ArrayList();
		while (true) {
			Scanner sc = new Scanner(System.in);
			System.out.println("Please choose:Add/Remove/Search/Replace/EXIT");
			String action = sc.nextLine();
			if (action.equals("Add")) {
				String firstname = null;
				String lastname = null;
				String phoneNumber = null;
				String street = null;
				String number = null;
				String city = null;
				String country = null;
				boolean value = true;
				while (value) {

					boolean value0 = true;
					while (value0) {
						sc = new Scanner(System.in);
						System.out.println("Please enter the phone number");
						phoneNumber = sc.nextLine();
						sc = new Scanner(System.in);
						System.out.println("Are you sure?:" + phoneNumber);
						System.out.println("Yes/No");
						String verification = sc.nextLine();
						if (verification.equals("Yes")) {
							value0 = false;
						} else {
							value0 = true;
						}
					}
					value = false;
				}
				value = true;
				boolean value1 = true;
				while (value1) {
					sc = new Scanner(System.in);
					System.out.println("Please enter the first name:");
					firstname = sc.nextLine();
					sc = new Scanner(System.in);
					System.out.println("Please enter the last name:");
					lastname = sc.nextLine();
					value1 = false;
					sc = new Scanner(System.in);
					System.out.println("Are you sure?:" + firstname + " " + lastname);
					System.out.println("Yes/No");
					String verification = sc.nextLine();
					if (verification.equals("Yes")) {
						value1 = false;
					} else {
						value1 = true;
					}
					boolean value2 = true;
					while (value2) {
						sc = new Scanner(System.in);
						System.out.println("Please enter the address:");
						System.out.println("Street:");
						street = sc.nextLine();
						sc = new Scanner(System.in);

						System.out.println("Number:");
						number = sc.nextLine();
						sc = new Scanner(System.in);

						System.out.println("City:");
						city = sc.nextLine();
						sc = new Scanner(System.in);

						System.out.println("Country:");
						country = sc.nextLine();
						value2 = false;
						sc = new Scanner(System.in);
						System.out.println("Are you sure?:" + street + " " + number + " " + city + " " + country);
						;
						System.out.println("Yes/No");
						verification = sc.nextLine();
						if (verification.equals("Yes")) {
							value2 = false;
						} else {
							value2 = true;
						}
					}

				}

				Details object = new Details(phoneNumber, lastname, firstname, street, number, city, country);
				array.add(object);
				System.out.println(array);
			} else if (action.equals("Remove")) {
				String phoneNumber = null;
				boolean value = true;
				while (value) {
					try {
						boolean value0 = true;
						while (value0) {
							sc = new Scanner(System.in);
							System.out.println("Please enter the phone number you want to remove:");
							phoneNumber = sc.nextLine();
							sc = new Scanner(System.in);
							System.out.println("Are you sure you want to delete this?:" + phoneNumber);
							System.out.println("Yes/No");
							String verification = sc.nextLine();
							if (verification.equals("Yes")) {
								value0 = false;
							} else {
								value0 = true;
							}

						}
						System.out.println(phoneNumber);
						Output p = new Output(array);
						p.remove(phoneNumber);

						value = false;
					} catch (InputMismatchException e) {
						System.out.println("Please enter a valid number!");
					}
				}

			} else if (action.equals("Search")) {

				sc = new Scanner(System.in);
				System.out.println("Please choose by:Name/Phone Number/Address/EXIT");
				String choice = sc.nextLine();
				String phoneNumber = null;
				if (choice.equals("Phone Number")) {
					boolean value = true;
					while (value) {
						try {
							boolean value0 = true;
							while (value0) {
								sc = new Scanner(System.in);
								System.out.println("Please enter the phone number");
								phoneNumber = sc.nextLine();
								sc = new Scanner(System.in);
								System.out.println("Are you sure?:" + phoneNumber);
								System.out.println("Yes/No");
								String verification = sc.nextLine();
								if (verification.equals("Yes")) {
									value0 = false;
								} else {
									value0 = true;
								}
							}
							value = false;
						} catch (InputMismatchException e) {
							System.out.println("Please enter a valid number!");
						}
					}

					Output phone = new Output(array);
					phone.search(phoneNumber, 1, -1, 0, null);

					// Pattern thePattern = Pattern.compile(;
					// Matcher m = thePattern.matcher(theStringToTest);

				} else if (choice.equals("Name")) {
					String string = null;
					boolean value1 = true;
					while (value1) {
						sc = new Scanner(System.in);
						System.out.println("Please enter the first name:");
						String firstname = sc.nextLine();
						sc = new Scanner(System.in);
						System.out.println("Please enter the last name:");
						String lastname = sc.nextLine();
						value1 = false;
						sc = new Scanner(System.in);
						System.out.println("Are you sure?:" + firstname + " " + lastname);
						System.out.println("Yes/No");
						String verification = sc.nextLine();
						if (verification.equals("Yes")) {
							value1 = false;
							string = firstname + " " + lastname;

						} else {
							value1 = true;
						}

					}
					Output out = new Output(array);
					out.search(string, 2, 2, 0, null);

				} else if (choice.equals("Address")) {
					sc = new Scanner(System.in);
					System.out.println("Please choose by:Street/Number/City/Country/EXIT");
					String choice3 = sc.nextLine();
					boolean value2 = true;
					String string = null;
					while (value2) {

						sc = new Scanner(System.in);
						System.out.println("Please enter the address:");
						System.out.println("Street:");
						String street = sc.nextLine();
						sc = new Scanner(System.in);

						System.out.println("Number:");
						String number = sc.nextLine();
						sc = new Scanner(System.in);

						System.out.println("City:");
						String city = sc.nextLine();
						sc = new Scanner(System.in);

						System.out.println("Country:");
						String country = sc.nextLine();
						value2 = false;
						sc = new Scanner(System.in);
						System.out.println("Are you sure?:" + street + " " + number + " " + city + " " + country);
						;
						System.out.println("Yes/No");
						String verification = sc.nextLine();
						if (verification.equals("Yes")) {
							value2 = false;
							string = street + " " + number + " " + city + " " + country;
						} else {
							value2 = true;
						}
					}
					Output out = new Output(array);
					out.search(string, 3, 6, 0, null);
				} else if (choice.equalsIgnoreCase("EXIT")) {
					break;

				} else {
					System.out.println("Please try again!");
				}

			} else if (action.equals("Replace")) {
				sc = new Scanner(System.in);
				System.out.println("Please choose search by:Name/Phone Number/Address/EXIT");
				String choice3 = sc.nextLine();
				String phoneNumber = null;
				if (choice3.equals("Phone Number")) {
					boolean value = true;
					while (value) {
						try {
							boolean value0 = true;
							while (value0) {
								sc = new Scanner(System.in);
								System.out.println("Please enter the phone number");
								phoneNumber = sc.nextLine();
								sc = new Scanner(System.in);
								System.out.println("Are you sure?:" + phoneNumber);
								System.out.println("Yes/No");
								String verification = sc.nextLine();
								if (verification.equals("Yes")) {
									value0 = false;
								} else {
									value0 = true;
								}
							}
							value = false;
						} catch (InputMismatchException e) {
							System.out.println("Please enter a valid number!");
						}
					}
					sc = new Scanner(System.in);
					System.out.println("Please enter the new phone number:");
					String choice4 = sc.nextLine();

					Output phone = new Output(array);

					phone.search(phoneNumber, 1, -1, 1, choice4);
				} else if (choice3.equals("Name")) {
					String string = null;
					boolean value1 = true;
					while (value1) {
						sc = new Scanner(System.in);
						System.out.println("Please enter the first name:");
						String firstname = sc.nextLine();
						sc = new Scanner(System.in);
						System.out.println("Please enter the last name:");
						String lastname = sc.nextLine();
						value1 = false;
						sc = new Scanner(System.in);
						System.out.println("Are you sure?:" + firstname + " " + lastname);
						System.out.println("Yes/No");
						String verification = sc.nextLine();
						if (verification.equals("Yes")) {
							value1 = false;
							string = firstname + " " + lastname;

						} else {
							value1 = true;
						}

					}
					sc = new Scanner(System.in);
					System.out.println("Please enter the new phone number:");
					String choice4 = sc.nextLine();

					Output out = new Output(array);
					out.search(string, 2, 2, 1, choice4);

				} else if (choice3.equals("Address")) {
					sc = new Scanner(System.in);
					System.out.println("Please choose by:Street/Number/City/Country/EXIT");
					choice3 = sc.nextLine();
					boolean value2 = true;
					String string = null;
					while (value2) {

						sc = new Scanner(System.in);
						System.out.println("Please enter the address:");
						System.out.println("Street:");
						String street = sc.nextLine();
						sc = new Scanner(System.in);

						System.out.println("Number:");
						String number = sc.nextLine();
						sc = new Scanner(System.in);

						System.out.println("City:");
						String city = sc.nextLine();
						sc = new Scanner(System.in);

						System.out.println("Country:");
						String country = sc.nextLine();
						value2 = false;
						sc = new Scanner(System.in);
						System.out.println("Are you sure?:" + street + " " + number + " " + city + " " + country);
						;
						System.out.println("Yes/No");
						String verification = sc.nextLine();
						if (verification.equals("Yes")) {
							value2 = false;
							string = street + " " + number + " " + city + " " + country;
						} else {
							value2 = true;
						}
					}
					sc = new Scanner(System.in);
					System.out.println("Please enter the new phone number:");
					String choice4 = sc.nextLine();

					Output phone = new Output(array);

					Output out = new Output(array);
					out.search(string, 3, 6, 1, choice4);
				} else if (choice3.equalsIgnoreCase("EXIT")) {
					break;

				} else {
					System.out.println("Please try again!");
				}

			} else if (action.equalsIgnoreCase("EXIT")) {
				System.out.println("Thank you for using our application!");
				break;
			} else {
				System.out.println("Please try again!");
			}
		}

	}
}
