package Collections2;

import java.security.KeyStore.Entry;
import java.util.Scanner;
import java.util.TreeMap;

public class CountryApp {
	public static void main(String args[]) {
		TreeMap<String, Country> map = new TreeMap();
		TreeMap<String, Country> map2 = new TreeMap();
		while (true) {
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter the name:");
			String name = sc.nextLine();
			if (name.equalsIgnoreCase("EXIT")) {
				break;
			}
			sc = new Scanner(System.in);
			System.out.println("Enter the continent name:");
			String continentname = sc.nextLine();
			sc = new Scanner(System.in);
			System.out.println("Enter the population:");
			double population = sc.nextDouble();
			Country c1 = new Country(name, continentname, population);
			map.put(c1.name, c1);
		}

		for (java.util.Map.Entry<String, Country> m : map.entrySet()) {
			System.out.println(m);
		}
		while (true) {
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter the value you want to find:");
			String value = sc.nextLine();
			if (map.containsKey(value)) {
				System.out.println("The value exists!");
				break;
			} else {
				System.out.println("Not here");
			}
		}
		while (true) {
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter the key:");
			String key = sc.nextLine();
			if (map.containsKey(key)) {
				System.out.println(map.lowerKey(key));
				break;
			} else {
				System.out.println("Not here");
			}
		}

		TreeMap<String, Country> map3 = new TreeMap();
		map3 = (TreeMap<String, Country>) map.clone();

		System.out.println(map3);

		while (true) {
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter the name:");
			String name = sc.nextLine();
			if (name.equalsIgnoreCase("EXIT")) {
				break;
			}
			sc = new Scanner(System.in);
			System.out.println("Enter the continent name:");
			String continentname = sc.nextLine();
			sc = new Scanner(System.in);
			System.out.println("Enter the population:");
			double population = sc.nextDouble();
			Country c1 = new Country(name, continentname, population);
			map2.put(c1.name, c1);
		}

		map.entrySet().retainAll(map2.entrySet());
		System.out.println("Intersection:" + map);
		map.putAll(map2);
		System.out.println("Reunion:" + map);
	}
}
