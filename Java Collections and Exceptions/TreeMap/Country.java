package Collections2;

import java.security.KeyStore.Entry;

public class Country {
	String name, continentname;
	double population;

	public Country(String name, String continentname, double population) {
		super();
		this.name = name;
		this.continentname = continentname;
		this.population = population;
	}

	@Override
	public String toString() {
		return "Country [name=" + name + ", continentname=" + continentname + ", population=" + population + "]";
	}
}
