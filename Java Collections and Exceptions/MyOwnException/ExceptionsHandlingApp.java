package exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionsHandlingApp {
	public static void main(String args[])  {
		try {
		Scanner sc=new Scanner(System.in);
		int number=sc.nextInt();
		if(number>20000000) {
			throw new NumberTooBigException();
		}
		System.out.println(number);}
		catch(InputMismatchException e){
			System.out.println("Enter a number!");
		} catch (NumberTooBigException e) {
			System.out.println("Numarul este prea mare");
			e.printStackTrace();
		}
	}
}
