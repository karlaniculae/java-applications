package CoffeeRestaurantConsumption;

public class MyException extends Exception {
	public MyException(String msg) {
		super(msg);
		System.out.println(msg);
	}
}
