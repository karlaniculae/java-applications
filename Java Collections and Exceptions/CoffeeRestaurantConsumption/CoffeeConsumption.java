package CoffeeRestaurantConsumption;

import java.util.Random;
import java.util.Scanner;

public class CoffeeConsumption {

	public void coffee(double n) throws MyException {
		Random m = new Random();
		int q = m.nextInt(100);

		if (n < 35) {
			throw new MyException("The manager is called!");
		} else if (n > 70) {
			throw new MyException("the client asks for a glass of water");
		} else if ((n > 35 && n < 70) && (q > 0 && q < 25)) {
			throw new MyException("The client left!");
		} else {
			System.out.println("The client is feeling ok");
		}
	}

	public static void main(String args[]) {
		try {
			Scanner sc = new Scanner(System.in);
			System.out.println("Please enter the temperature of the coffee");
			double q = sc.nextDouble();
			CoffeeConsumption c = new CoffeeConsumption();
			c.coffee(q);
		} catch (MyException e) {
		} catch (Exception e) {
			System.out.println("Enetr a number!");
		}
	}
}
