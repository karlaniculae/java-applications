package Exceptions2;

public class ThrowExceptionApp {
	public void g() throws MyException {
		throw new MyException("Something is wrong!");
	}

	public void f() throws SecondException {
		try {
			g();

		} catch (MyException e) {
			System.out.println("First exception");
			throw new SecondException();

		}
	}

	public static void main(String args[]) {

		try {
			ThrowExceptionApp object = new ThrowExceptionApp();
			object.f();

		} catch (SecondException e) {
			System.out.println("Second exception");
		}
	}
}
