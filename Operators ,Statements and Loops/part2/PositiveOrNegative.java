package tema2;

import java.util.Scanner;

public class PositiveOrNegative {

		public static void main (String[] args) {
			Scanner sc=new Scanner(System.in);
			System.out.println("Please enter the number:");
			int nr=sc.nextInt();
			if(nr<0) 
				System.out.println("Negative");
			else
				System.out.println("Positive");
		}

}
