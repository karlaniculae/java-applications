package tema2;

import java.util.Scanner;

public class OddNumbers {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Input number of terms is:");
		int number =sc.nextInt();
		int sum=0;
		int odd=1,ct=0;
		System.out.println("The odd numbers are :");
		while(ct<number) {
			System.out.println(odd);
			sum+=odd;
			odd+=2;
			ct++;
		}
	
		System.out.println("The Sum of odd Natural Number up to"+number+ " terms is:"+sum);
		
}
}
